// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// Splynx admin info
const LOGIN = '';
const PASSWORD = '';

// Create new api object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// Enable debug mode
// api.debug = true;

api.login(SplynxApi.LOGIN_TYPE_ADMIN, {
    login: LOGIN,
    password: PASSWORD
}).then(() => {
    // Get customer
    api.get('admin/customers/customer', 1).then(res => {
        console.log('Customer:', res);
    }).catch(err => {
        console.log('Error ', err);
    });
}).catch(err => {
    console.log('Error', err);
});
